#![feature(dbg_attribute)]
use nom::{eol, is_alphanumeric, recognize_float, anychar, types::CompleteByteSlice};

use net::IpAddr;
use std::{str, collections::HashMap};

#[derive(Debug)]
pub enum NatsServerMessage {
    PING,
    PONG,
    OK,
    INFO(InfoMessage),
}

#[derive(Debug)]
pub struct InfoMessage {
    server_id: String,
    version: String,
    host: IpAddr,
    port: u16,
    auth_required: bool,
    tls_required: bool,
    tls_verify: bool,
    max_payload: u64,
}

// INFO {"server_id":"JUOufISZqT10TSan4iBHCJ","version":"1.0.6","git_commit":"","go":"go1.9.4","host":"0.0.0.0","port":4222,"auth_required":false,"tls_required":false,"tls_verify":false,"max_payload":1048576}

#[derive(Debug, PartialEq)]
pub enum JsonValue {
    Str(String),
    Num(f32),
    Array(Vec<JsonValue>),
    Bool(bool),
    Object(HashMap<String, JsonValue>),
}

named!(float<f32>, flat_map!(recognize_float, parse_to!(f32)));

named!(json_bool<bool>, flat_map!(alt!(tag!("true") | tag!("false")), parse_to!(bool)));

named!(
    string<&str>,
    delimited!(
        tag!("\""),
        map_res!(
            escaped!(none_of!("\\\""), '\\', one_of!("\"n\\")),
            str::from_utf8
            ),
            tag!("\"")
            )


);

#[test]
fn test_qtstring() {
    let test = br#""he_l\"l\no""#;
    let res = string(test);
    println!("{:?}", res.unwrap())
}

named!(
    array<Vec<JsonValue>>,
    ws!(delimited!(
            char!('['),
            separated_list!(char!(','), value),
            char!(']')
    ))
);

named!(
    key_value<(&str, JsonValue)>,
    ws!(separated_pair!(string, char!(':'), value))
);

named!(kvs<Vec<(&str, JsonValue)>>,
    delimited!(char!('{'), ws!(separated_list!(char!(','), key_value)), char!('}'))
);

named!(
    hash<HashMap<String, JsonValue>>,
    map!(terminated!(delimited!(opt!(is_a!(" \t")),kvs, opt!(is_a!(" \t"))), alt!(eol | tag!(","))), |tuple_vec| {
        let mut h = HashMap::new();
        for (k,v) in tuple_vec {
            h.insert(String::from(k), v);
        }
        h
    })
);

#[test]
fn hash_test() {
    let test = b"  {\"server_id\":\"JUOufISZqT09TSan4iBHCJ\",\"version\":\"1.0.6\",\"git_commit\":\"\",\"go\":\"go1.9.4\",\"host\":\"0.0.0.0\",\"port\":4222,\"auth_required\":false,\"tls_required\":false,\"tls_verify\":false,\"max_payload\":1048576}  \r\n";

    //FIXME: top level value must be an object?
    println!("{:?}", hash(test));
    //assert!(false);
}

named!(
    value<JsonValue>,
    ws!(alt!(
            string    => { |s| JsonValue::Str(String::from(s)) } |
            float     => { |f| JsonValue::Num(f) } |
            array     => { |a| JsonValue::Array(a) } |
            //hash      => { |h| JsonValue::Object(h) } |
            json_bool => { |b| JsonValue::Bool(b) }
    ))
);

named!(ping_msg<NatsServerMessage>,
       map!(tag!("PING"), |_| NatsServerMessage::PING));

named!(pong_msg<NatsServerMessage>,
       map!(tag!("PONG"), |_| NatsServerMessage::PONG));

named!(ok_msg<NatsServerMessage>,
       map!(tag!("+OK"), |_| NatsServerMessage::OK));

named!(pub nats_server_msg<NatsServerMessage>,
       terminated!(alt!(ping_msg | pong_msg | ok_msg), eol));

#[cfg(test)]
mod tests {
    use super::*;
    use nom::Err::*;

    #[test]
    fn parse_ping_msg() {
        assert_eq!(nats_server_msg(b"PING\r\n").is_ok(), true)
    }

    #[test]
    fn parse_pong() {
        assert_eq!(nats_server_msg(b"PONG\n").is_ok(), true)
    }

    #[test]
    fn parse_ok_msg() {
        assert_eq!(nats_server_msg(b"+OK\n").is_ok(), true)
    }

    #[test]
    fn incompl() {
        let res = nats_server_msg(b"P");
        match res {
            Err(e) => match e {
                Incomplete(_) => (),
                x => panic!("not an incomplete {:?}", x),
            },
            x => panic!("not an incomplete {:?}", x),
        }
    }

    #[test]
    fn offset() {
        let msg = b"PING\r\n";
        if let Ok((rst, res)) = nats_server_msg(*&msg) {
            println!("rst: {:?}, res: {:?}", rst, res);
            let offset = msg.len() - rst.len();
            assert_eq!(offset, 6)
        } else {
            panic!("error, not ok");
        }
    }



}
