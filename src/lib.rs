#![crate_type = "lib"]

extern crate bytes;
#[macro_use]
extern crate futures;
#[macro_use]
extern crate nom;
extern crate tokio;
use tokio::codec::{Decoder, Encoder, Framed};
use tokio::net::{ConnectFuture, TcpStream};
use std::net::SocketAddr;
use tokio::prelude::*;
use std::*;
mod client_error;
use client_error::*;
mod parser;
use parser::*;
mod client;

enum NatsMessageCodecState {
    Header(usize), // current index in header
    Body(usize),   // remaining bytes
}

pub struct NatsMessageCodec {
    state: NatsMessageCodecState,
}

impl NatsMessageCodec {
    pub fn new() -> Self {
        use self::NatsMessageCodecState::*;
        NatsMessageCodec { state: Header(0) }
    }
}

impl Decoder for NatsMessageCodec {
    type Item = NatsServerMessage;
    type Error = ClientError;
    fn decode(&mut self, src: &mut bytes::BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        use NatsMessageCodecState::*;
        match self.state {
            Header(_p) => {
                match nats_server_msg(&src[..]) {
                    Ok((_, msg)) => {
                        // add code for body consumption
                        self.state = Header(0);
                        Ok(Some(msg))
                    }
                    Err(nom_err) => match nom_err {
                        nom::Err::Incomplete(_) => Ok(None),
                        nom::Err::Error(_) => Ok(None),
                        nom::Err::Failure(ctx) => Err(ClientError::ErrParse(
                            format!("error parsing: {:?}", ctx).to_string(),
                        )),
                    },
                }
            }
            Body(_br) => Ok(None),
        }
    }
}

pub struct NatsClientMessage {}

impl Encoder for NatsMessageCodec {
    type Item = NatsClientMessage;
    type Error = io::Error;
    fn encode(&mut self, _item: Self::Item, _dst: &mut bytes::BytesMut) -> Result<(), Self::Error> {
        Ok(())
    }
}

#[must_use = "futures do nothing unless polled"]
pub struct TransportFuture {
    inner: ConnectFuture,
}

impl TransportFuture {
    pub fn new(f: ConnectFuture) -> Self {
        TransportFuture { inner: f }
    }
}

impl Future for TransportFuture {
    type Item = Framed<TcpStream, NatsMessageCodec>;
    type Error = io::Error;
    fn poll(&mut self) -> Result<Async<Framed<TcpStream, NatsMessageCodec>>, io::Error> {
        let strm = try_ready!(self.inner.poll());
        Ok(Async::Ready(Framed::new(strm, NatsMessageCodec::new())))
    }
}

#[cfg(test)]
mod tests {
    use std::net::{AddrParseError, SocketAddr};
    use std::result::*;
    use super::*;

    #[test]
    fn parse_socket_addr() {
        let addr: Result<SocketAddr, AddrParseError> = "127.0.0.1:4222".parse();
        assert_eq!(addr.is_ok(), true);
    }

    #[test]
    fn trans_future() {
        let addr = "127.0.0.1:4222";
        let sa = addr.parse::<SocketAddr>().expect("unable to parse");
        let tf = TransportFuture::new(TcpStream::connect(&sa))
            .and_then(|f| {
                println!("connected");
                let cn = f.get_ref();
                cn.shutdown(std::net::Shutdown::Both)
            })
            .map_err(|e| panic!("error: {:?}", e));
        tokio::run(tf)
    }
}
