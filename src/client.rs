use std::sync::mpsc;

pub enum NatsClientMessage {}


pub struct NatsClient {
    recv: mpsc::Receiver<self::NatsClientMessage>,
    snd: mpsc::Sender<self::NatsClientMessage>,
}

impl NatsClient {
    pub fn new() -> Self {
        let (snd, recv) = mpsc::channel();
        NatsClient{recv: recv, snd: snd}
    }
}
