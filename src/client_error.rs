use std::*;
use std::net::AddrParseError;
#[derive(Debug, Clone)]
pub enum ClientError {
    ErrBadSocketAddr(AddrParseError),
    ErrIo(String),
    ErrParse(String),
}

impl fmt::Display for ClientError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::ClientError::*;
        match *self {
            ErrBadSocketAddr(ref se) => se.fmt(f),
            ErrIo(ref m) => write!(f, "io error: {}", m),
            ErrParse(ref m) => write!(f, "parse header error: {}", m),
        }
    }
}

impl error::Error for ClientError {
    fn description(&self) -> &str {
        use self::ClientError::*;
        match *self {
            ErrBadSocketAddr(ref se) => se.description(),
            //ErrConnect(_) => "Error connecting to nats",
            ErrParse(ref se) => se,
            ErrIo(ref e) => e,
        }
    }
    fn cause(&self) -> Option<&error::Error> {
        use self::ClientError::*;
        match *self {
            ErrBadSocketAddr(ref e) => Some(e),
            //ErrConnect(_) => None,
            ErrParse(_) => None,
            ErrIo(_) => None,
        }
    }
}

impl From<AddrParseError> for ClientError {
    fn from(err: AddrParseError) -> ClientError {
        ClientError::ErrBadSocketAddr(err)
    }
}

impl From<io::Error> for ClientError {
    fn from(err: io::Error) -> ClientError {
        ClientError::ErrIo(format!("{:?}", err).to_string())
    }
}
